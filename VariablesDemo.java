public class VariablesDemo{
   public static String name; //variable de clase, accedemo a traves de la clase
   private String lastname;// variable instancia, accedemos a traves de un objeto
   public static final float PI = 3.1416f;// variable constante 
    public static void main(String[] args) {
        int miEdad = 20; //variable local
        boolean isAdult = false;
        VariablesDemo vd = new VariablesDemo(); //variable local
        VariablesDemo.name = "Pepito";
        vd.lastname = "Perez";

        if(miEdad > 18){
            isAdult = true;
        }

        if(isAdult){
            System.out.println("Es mayor de edad!");
        }

        vd.print(miEdad);
        vd.print();
    }

    public void print(int age){ //variable parametro o argumento
        System.out.println(age);
    }

    public void print(){
        System.out.println(VariablesDemo.name);
        System.out.println(lastname);
    }

    
}

/*Convenciones de las clases (Obligatorio)
1. El nombre de la clase debe iniciar siempre en mayuscula
2. Si el nombre de la clase es compuesta, la primera letra
de la siguiente palabra inicia con mayuscula

public
protected
private
default

*/